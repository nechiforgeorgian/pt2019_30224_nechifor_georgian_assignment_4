package presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import businessLogic.MenuItemBusiness;
import model.MenuItem;

public class NewItem {

    NewItem() {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                initialize();
            }
        });
    }

    void initialize() {
        JFrame frame = new JFrame("New Menu Item");
        frame.setLayout(null);
        frame.setBounds(100, 100, 300, 250);
        frame.setVisible(true);

        JLabel nameLabel = new JLabel("Name: ");
        nameLabel.setBounds(20, 20, 70, 30);
        JLabel typeLabel = new JLabel("Type: ");
        typeLabel.setBounds(20, 60, 70, 30);
        JLabel priceLabel = new JLabel("Price: ");
        priceLabel.setBounds(20, 100, 70, 30);

        JTextField nameField = new JTextField();
        nameField.setBounds(70, 20, 200, 30);
        JTextField typeField = new JTextField();
        typeField.setBounds(70, 60, 200, 30);
        JTextField priceField = new JTextField();
        priceField.setBounds(70, 100, 200, 30);

        JButton finish = new JButton("Finish");
        finish.setBounds(50, 150, 200, 30);
        finish.setBackground(Color.decode("#3da7ff"));
        finish.setFocusPainted(false);
        //finish.setText("finish");
        finish.setForeground(Color.decode("#ffffff"));
        finish.setFont(new Font("Open Sans", Font.BOLD, 14));
        finish.setBorder(null);
        finish.setContentAreaFilled(false);
        finish.setOpaque(true);

        finish.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    MenuItem menuItem = new MenuItem(0, Integer.parseInt(typeField.getText()), nameField.getText(), Float.parseFloat(priceField.getText()));
                    new MenuItemBusiness().newMenuItem(menuItem, 1);
                    JOptionPane.showMessageDialog(frame, "MenuItem added", "Success", JOptionPane.INFORMATION_MESSAGE);
                    frame.dispose();
                    MenuItemList menuItemList = new MenuItemList();
                    menuItemList.setFlag(1);
                    menuItemList.initialize();
                } catch (Exception ex) {
                    ex.getMessage();
                    JOptionPane.showMessageDialog(frame, "Invalid inputs", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        frame.add(finish);
        frame.add(nameField);
        frame.add(typeField);
        frame.add(priceField);

        frame.add(nameLabel);
        frame.add(typeLabel);
        frame.add(priceLabel);

    }


    public static void main(String[] args) {
        new NewItem();
    }
}
