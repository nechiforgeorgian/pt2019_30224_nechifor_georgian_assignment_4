package presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class AdminUI {

    AdminUI() {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                initialize();
            }
        });
    }

    private void initialize() {
        JFrame frame = new JFrame();
        frame.setVisible(true);
        frame.setLayout(null);
        frame.setBounds(100, 100, 560, 180);
        frame.setTitle("Admin Menu");

        List<JButton> buttonList = new ArrayList<JButton>();
        buttonList.add(new JButton("new menuItem")); buttonList.get(0).setBounds(20, 25, 120, 50);
        buttonList.add(new JButton("edit menuItem")); buttonList.get(1).setBounds(150, 25, 120, 50);
        buttonList.add(new JButton("del menuItem")); buttonList.get(2).setBounds(280, 25, 120, 50);
        buttonList.add(new JButton("view menuItem")); buttonList.get(3).setBounds(410, 25, 120, 50);

        for(int i = 0; i < buttonList.size(); i++) {
            final int index = i+1;
            JButton j = buttonList.get(i);
            j.addActionListener(e ->
            {   MenuItemList menuItemList = new MenuItemList();
                menuItemList.setFlag(index);
                menuItemList.initialize();
            });
        }

        for(JButton j: buttonList) {
            j.setFocusPainted(false);
            frame.add(j);
        }

        JButton back = new JButton("Back");
        back.setBounds(205, 90, 150, 40);
        frame.add(back);

        back.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
                new FirstScreen();
            }
        });
    }


    public static void main(String[]args) {
        new AdminUI();
    }
}
