package presentation;

import businessLogic.ClientOrderBusiness;
import data.AbstractAccess;
import model.ClientOrder;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.lang.reflect.Field;

public class Orders {
    private JFrame frame;
    private JTable orderTable;
    private int flag;

    public void setFlag(int flag) {
        this.flag = flag;
    }

    Orders() {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                initialize();
            }
        });
    }

    private void initialize() {
        frame = new JFrame("Orders");
        frame.setBounds(100, 100, 700, 350);
        frame.setLayout(null);
        frame.setVisible(true);

        Task task = new Task();
        task.execute();

    }


    protected class Task extends SwingWorker<String, String> {
        int refresh = 0;

        @Override
        protected String doInBackground() {
            String[] arg = new String[ClientOrder.class.getDeclaredFields().length];
            int i = 0;
            for (Field field : ClientOrder.class.getDeclaredFields()) {
                arg[i++] = field.getName();
            }

            int rows = new ClientOrderBusiness().getNumber();
            DefaultTableModel model = new DefaultTableModel(arg, rows) {
                @Override
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };

            int index = 0;
            for (int j = 1; j <= rows; j++) {
                ClientOrder clientOrder = new AbstractAccess<>(ClientOrder.class).getColumnByID(j);
                model.setValueAt(clientOrder.getID(), index, 0);
                model.setValueAt(clientOrder.getOrderDate(), index, 1);
                model.setValueAt(clientOrder.getTableNumber(), index, 2);
                model.setValueAt(clientOrder.getTotalPrice(), index, 3);
                index++;
            }

            orderTable = new JTable();
            orderTable.setModel(model);
            orderTable.setBounds(50, 10, 600, 200);
            orderTable.setPreferredScrollableViewportSize(new Dimension(600, 200));

            DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
            cellRenderer.setHorizontalAlignment(JLabel.CENTER);
            for (int x = 0; x < orderTable.getColumnCount(); x++)
                orderTable.getColumnModel().getColumn(x).setCellRenderer(cellRenderer);

            orderTable.setCellSelectionEnabled(false);
            orderTable.setRowSelectionAllowed(true);
            orderTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
            orderTable.getTableHeader().setReorderingAllowed(false);

            frame.add(orderTable);

            JScrollPane pane = new JScrollPane(orderTable);
            pane.setBounds(50, 10, 602, 200);
            pane.setVisible(true);
            pane.getViewport().setBackground((Color.decode("#dedede")));
            frame.add(pane);

            if (refresh == 1) {
                model.setRowCount(0);
            }

            return null;
        }
    }

    public static void main(String[] args) {
        new Orders();
    }

}
