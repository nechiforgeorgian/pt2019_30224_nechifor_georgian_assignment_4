package presentation;

import businessLogic.AdminData;
import model.MenuItem;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class EditItem {
    private MenuItem selectedItem;

    EditItem(MenuItem menuItem) {
        this.selectedItem = menuItem;
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                initialize();
            }
        });
    }

    protected void initialize() {
        JFrame frame = new JFrame("Edit Item");
        frame.setVisible(true);
        frame.setBounds(100, 100, 300, 400);
        frame.setLayout(null);

        JLabel nameLabel = new JLabel("Name: ");
        nameLabel.setBounds(10, 10, 50, 30);
        JLabel typeLabel = new JLabel("Type: ");
        typeLabel.setBounds(10, 50, 50, 30);
        JLabel priceLabel = new JLabel("Price: ");
        priceLabel.setBounds(10, 90, 50, 30);

        frame.add(nameLabel);
        frame.add(typeLabel);
        frame.add(priceLabel);


        JTextField nameField = new JTextField();
        nameField.setBounds(60, 10, 200, 30);
        nameField.setText(selectedItem.getMenuItemName());

        JTextField typeField = new JTextField();
        typeField.setBounds(60, 50, 200, 30);
        typeField.setText(Integer.toString(selectedItem.getMenuItemType()));

        JTextField priceField = new JTextField();
        priceField.setBounds(60, 90, 200, 30);
        priceField.setText(Float.toString(selectedItem.getMenuItemPrice()));

        frame.add(nameField);
        frame.add(typeField);
        frame.add(priceField);

        JButton finish = new JButton("Finish");
        finish.setBounds(50, 150, 200, 30);
        finish.setBackground(Color.decode("#3da7ff"));
        finish.setFocusPainted(false);
        //finish.setText("finish");
        finish.setForeground(Color.decode("#ffffff"));
        finish.setFont(new Font("Open Sans", Font.BOLD, 14));
        finish.setBorder(null);
        finish.setContentAreaFilled(false);
        finish.setOpaque(true);

        frame.add(finish);

        finish.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    new AdminData().editMenuItem(new MenuItem(selectedItem.getID(), Integer.parseInt(typeField.getText()), nameField.getText(), Float.parseFloat(priceField.getText())));
                    JOptionPane.showMessageDialog(frame, "Edit Item successfully", "Succes", JOptionPane.INFORMATION_MESSAGE);
                    frame.dispose();
                    MenuItemList menuItemList = new MenuItemList();
                    menuItemList.setFlag(2);
                    menuItemList.initialize();
                } catch (Exception ex) {
                    ex.getMessage();
                    JOptionPane.showMessageDialog(frame, "Enter valid values.", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }

        });
    }

    public static void main(String[] args) {
        new EditItem(null);
    }
}
