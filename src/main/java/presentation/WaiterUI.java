package presentation;

import businessLogic.Observer;

import javax.swing.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import static java.awt.EventQueue.*;

public class WaiterUI {
    protected List<Observer> observers = new ArrayList<>();
    protected String state;

    public void addObserver(Observer observer) {
        this.observers.add(observer);
    }

    public void setState(String news) {
        this.state = news;
        for (Observer observer : observers) {
            observer.update(this.state);
        }
    }

    WaiterUI() {
        invokeLater(() -> {
//            initialize();
        });
    }

    void initialize() {
        JFrame frame = new JFrame("Waiter");
        frame.setBounds(100, 100, 415, 150);
        frame.setLayout(null);
        frame.setVisible(true);

        List<JButton> buttons = new ArrayList<>();
        buttons.add(new JButton("new Order"));
        buttons.get(0).setBounds(10, 10, 120, 50);
        buttons.add(new JButton("view Orders"));
        buttons.get(1).setBounds(140, 10, 120, 50);
        buttons.add(new JButton("compute Bill"));
        buttons.get(2).setBounds(270, 10, 120, 50);
        buttons.add(new JButton("Back"));
        buttons.get(3).setBounds(140, 70, 120, 30);

        buttons.get(0).addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
//              frame.dispose();
                MenuItemList menuItemList = new MenuItemList();
                menuItemList.setFlag(5);
                WaiterUI waiter = new WaiterUI();
                waiter.state = state;
                waiter.observers = observers;
                menuItemList.setWaiter(waiter);
                menuItemList.initialize();
            }
        });
        buttons.get(1).addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new Orders();
            }
        });
        buttons.get(2).addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //new WaiterData().generateBill(new ClientOrder());
            }
        });
        buttons.get(3).addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
                new FirstScreen();
            }
        });

        for (JButton b : buttons) {
            b.setFocusPainted(false);
            frame.add(b);
        }
    }
}
