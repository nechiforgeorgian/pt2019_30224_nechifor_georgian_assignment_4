package presentation;

import businessLogic.AdminData;
import model.MenuItem;

import javax.swing.*;
import java.awt.*;

public class DeleteItem {
    private MenuItem selectedItem;

    DeleteItem(MenuItem selectedItem) {
        this.selectedItem = selectedItem;

        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                initialize();
            }
        });
    }

    private void initialize() {
        JFrame frame = new JFrame();
        new AdminData().deleteMenuItem(selectedItem);
        JOptionPane.showMessageDialog(frame, "MenuItem deleted successfully!", "Success", JOptionPane.INFORMATION_MESSAGE);
        frame.dispose();
        MenuItemList menuItemList = new MenuItemList();
        menuItemList.setFlag(3);
        menuItemList.initialize();
    }
}
