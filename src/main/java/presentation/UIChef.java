package presentation;

import businessLogic.Observer;

import javax.swing.*;
import java.awt.*;


public class UIChef implements Observer {
    private JTextArea orders = new JTextArea();

    @Override
    public void update(String str) {
        this.orders.setText(str);
    }

    public String getState() {
        return orders.getText();
    }


    UIChef() {

        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {

//                initialize();
            }
        });
    }

    void initialize() {
        JFrame frame = new JFrame();
        frame.setBounds(850, 100, 400, 280);
        frame.setVisible(true);
        frame.setLayout(null);
        frame.setTitle("Chef Orders");;

        orders.setBounds(50, 10, 300, 200);
        orders.setEditable(false);
        orders.setAlignmentX(JLabel.CENTER);
        orders.setAlignmentY(JLabel.CENTER);
        frame.add(orders);
    }


    public static void main(String[] args) {
        UIChef uiChef = new UIChef();
        uiChef.initialize();
    }
}
