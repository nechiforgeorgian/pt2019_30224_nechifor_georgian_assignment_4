package presentation;

import businessLogic.MenuItemBusiness;
import data.AbstractAccess;
import businessLogic.WaiterData;
import model.ClientOrder;
import model.ClientOrderDetails;
import model.MenuItem;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class MenuItemList {
    private int flag;
    private JFrame frame;
    private MenuItem selectedItem;
    private List<ClientOrderDetails> orderList;
    private JTable menuItems = new JTable();
    private UIChef observer;
    private WaiterUI observable;

    /**
     * @param flag sets the operation ex: flag = 1 -> edit menuItem
     */
    void setFlag(int flag) {
        this.flag = flag;
    }

    void setWaiter(WaiterUI waiter) {
        this.observable = waiter;
    }

    MenuItemList() {
        super();
        orderList = new ArrayList<>();
    }

    void initialize() {
        if (flag == 5) {
            observer = new UIChef();
            observer.initialize();
        }
        frame = new JFrame();
        String title;
        switch (flag) {
            case 1:
                title = "New Item";
                break;
            case 2:
                title = "Edit Item";
                break;
            case 3:
                title = "Delete Item";
                break;
            case 4:
                title = "View Items";
                break;
            case 5:
                title = "New Order";
                break;
            default:
                title = "";
                break;
        }

        frame.setTitle("Menu Item List: " + title);
        frame.setBounds(100, 100, 700, 350);
        frame.setVisible(true);
        frame.setLayout(null);
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.getContentPane().setBackground(Color.decode("#d6d6d6"));
        frame.requestFocusInWindow();

        new Task().execute();

        JButton button = new JButton(title);
        button.setBounds(50, 230, 120, 40);
        button.setBackground(Color.decode("#3da7ff"));
        button.setFocusPainted(false);
        //button.setText("Button");
        button.setForeground(Color.decode("#ffffff"));
        button.setFont(new Font("Open Sans", Font.BOLD, 14));
        button.setBorder(null);
        button.setContentAreaFilled(false);
        button.setOpaque(true);

        if (flag != 4)
            frame.add(button);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    switch (flag) {
                        case 1:
                            actionListener(NewItem.class);
                            break;
                        case 2:
                            selectedItem = new AbstractAccess<>(MenuItem.class).getColumnByID(Integer.parseInt(menuItems.getModel().getValueAt(menuItems.getSelectedRow(), 0).toString()));
                            actionListener(EditItem.class);
                            break;
                        case 3:
                            selectedItem = new AbstractAccess<>(MenuItem.class).getColumnByID(Integer.parseInt(menuItems.getModel().getValueAt(menuItems.getSelectedRow(), 0).toString()));
                            actionListener(DeleteItem.class);
                            break;
                        case 5:
                            for (int i = 0; i < menuItems.getRowCount(); i++) {
                                if (menuItems.getModel().getValueAt(i, 0) != null) {
                                    int amount = Integer.parseInt(menuItems.getModel().getValueAt(i, 4).toString());
                                    if (amount > 0) {
                                        MenuItem menuItem = new AbstractAccess<>(MenuItem.class).getColumnByID(Integer.parseInt(menuItems.getModel().getValueAt(i, 0).toString()));
                                        ClientOrderDetails orderDetails = new ClientOrderDetails(0, 0, menuItem.getID(), amount, menuItem.getMenuItemPrice());
                                        orderList.add(orderDetails);
                                        menuItems.getModel().setValueAt(0, i, 4);
                                    }
                                }
                            }
                            if (orderList.size() > 0) {
                                int tableNumber = Integer.parseInt(JOptionPane.showInputDialog("Table number: "));
                                new WaiterData().newOrder(new ClientOrder(0, new Date(2L), tableNumber, 0.0f), orderList);
                                JOptionPane.showMessageDialog(frame, "Order added.", "Success", JOptionPane.INFORMATION_MESSAGE);

                                String state = new String();
                                for (ClientOrderDetails str : orderList) {
                                    state += str.getMenuItemAmount() + " x " + new AbstractAccess<>(MenuItem.class).getColumnByID(str.getMenuItemID()).getMenuItemName() + " Masa: " + tableNumber + "\n";
                                }

                                observable.addObserver(observer);
                                observable.setState(state);
                                assert (observer.getState().equals(state));

                                //frame.dispose();
                            } else {
                                JOptionPane.showMessageDialog(frame, "Select a row for processing.", "Error", JOptionPane.ERROR_MESSAGE);
                            }
                            break;
                        default:
                            break;
                    }
                } catch (ArrayIndexOutOfBoundsException ex) {
                    JOptionPane.showMessageDialog(frame, "Select a row for processing.", "Error", JOptionPane.ERROR_MESSAGE);
                } catch (NumberFormatException nr) {
                    JOptionPane.showMessageDialog(frame, "Enter a correct table number", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
    }

    /**
     * table with menu items
     */
    protected class Task extends SwingWorker<String, String> {
        int refresh = 0;

        @Override
        protected String doInBackground() {
            String[] arg;
            if (flag == 5)
                arg = new String[MenuItem.class.getDeclaredFields().length + 1];
            else {
                arg = new String[MenuItem.class.getDeclaredFields().length];
            }
            int i = 0;
            for (Field field : MenuItem.class.getDeclaredFields()) {
                arg[i++] = field.getName();
            }
            if (flag == 5) {
                arg[i] = "Amount";
            }
            int rows = new MenuItemBusiness().getNumber();
            DefaultTableModel model = new DefaultTableModel(arg, rows) {
                @Override
                public boolean isCellEditable(int row, int column) {
                    if (column != 4)
                        return false;
                    return true;
                }
            };

            int index = 0;
            for (int j = 1; j <= rows; j++) {
                MenuItem menuItem = new AbstractAccess<>(MenuItem.class).getColumnByID(j);
                if (!menuItem.getMenuItemName().equals("null")) {
                    model.setValueAt(menuItem.getID(), index, 0);
                    model.setValueAt(menuItem.getMenuItemName(), index, 1);
                    if (1 == menuItem.getMenuItemType()) {
                        model.setValueAt("Base product", index, 2);
                    } else model.setValueAt("Compose product", index, 2);
                    model.setValueAt(menuItem.getMenuItemPrice(), index, 3);
                    if (flag == 5)
                        model.setValueAt(0, index, 4);
                    index++;
                }
            }

            menuItems.setModel(model);
            menuItems.setBounds(50, 10, 600, 200);
            menuItems.setPreferredScrollableViewportSize(new Dimension(600, 200));

            DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
            cellRenderer.setHorizontalAlignment(JLabel.CENTER);
            for (int x = 0; x < menuItems.getColumnCount(); x++)
                menuItems.getColumnModel().getColumn(x).setCellRenderer(cellRenderer);

            menuItems.setCellSelectionEnabled(false);
            menuItems.setRowSelectionAllowed(true);
            menuItems.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
            menuItems.getTableHeader().setReorderingAllowed(false);

            if (flag == 5) {
                menuItems.getColumnModel().getColumn(0).setPreferredWidth(75);
                menuItems.getColumnModel().getColumn(1).setPreferredWidth(200);
                menuItems.getColumnModel().getColumn(2).setPreferredWidth(200);
                menuItems.getColumnModel().getColumn(3).setPreferredWidth(75);
                menuItems.getColumnModel().getColumn(4).setPreferredWidth(50);
            } else {
                menuItems.getColumnModel().getColumn(0).setPreferredWidth(100);
                menuItems.getColumnModel().getColumn(1).setPreferredWidth(200);
                menuItems.getColumnModel().getColumn(2).setPreferredWidth(200);
                menuItems.getColumnModel().getColumn(3).setPreferredWidth(100);
            }
            frame.add(menuItems);

            JScrollPane pane = new JScrollPane(menuItems);
            pane.setBounds(50, 10, 602, 200);
            pane.setVisible(true);
            pane.getViewport().setBackground((Color.decode("#dedede")));
            frame.add(pane);

            if (refresh == 1) {
                model.setRowCount(0);
            }
            return null;
        }
    }

    private <T> void actionListener(Class<T> type) {
        try {
            if (type.getSimpleName().equalsIgnoreCase("EditItem")) {
                new EditItem(selectedItem);
            } else if (type.getSimpleName().equalsIgnoreCase("DeleteItem")) {
                new DeleteItem(selectedItem);
            } else {
                type.getDeclaredConstructor().newInstance();
            }
            frame.dispose();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException ex) {
            ex.printStackTrace();
        }
    }
}
