package presentation;

import data.Restaurant;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;

public class FirstScreen {

    FirstScreen() {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                initialize();
            }
        });
    }

    private void initialize() {

        JFrame frame = new JFrame("Restaurant");
        frame.setBounds(100, 100, 300, 150);
        frame.setVisible(true);
        frame.setLayout(null);

        try {
            new Restaurant().addMenu();
        } catch (FileNotFoundException e) {
            e.getMessage();
            JOptionPane.showMessageDialog(frame, "File with menu items invalid", "ERROR", JOptionPane.ERROR_MESSAGE);
            frame.dispose();
            return;
        }
        JButton admin = new JButton("Admin");
        admin.setBounds(30, 30, 100, 50);
        admin.setContentAreaFilled(false);
        admin.setOpaque(true);
        admin.setBackground(Color.decode("#3da7ff"));
        admin.setFocusPainted(false);
        admin.setForeground(Color.WHITE);
        frame.add(admin);

        JButton waiter = new JButton("Waiter");
        waiter.setBounds(160, 30, 100, 50);
        waiter.setContentAreaFilled(false);
        waiter.setOpaque(true);
        waiter.setBackground(Color.decode("#3da7ff"));
        waiter.setFocusPainted(false);
        waiter.setForeground(Color.WHITE);
        frame.add(waiter);

        admin.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
                new AdminUI();
            }
        });

        waiter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
                new WaiterUI().initialize();
            }
        });
    }

    public static void main(String[] args) {
        new FirstScreen();
    }
}
