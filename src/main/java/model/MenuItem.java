package model;
import java.io.Serializable;

public class MenuItem implements Serializable {
    private int ID;
    private int menuItemType;
    private String menuItemName;
    private float menuItemPrice;

    @Override
    public boolean equals(Object obj) {
        if(this == obj)
            return true;
        if(obj == null || obj.getClass() != this.getClass())
            return false;
        MenuItem menuItem = (MenuItem) obj;
        return (menuItem.menuItemName.equalsIgnoreCase(this.menuItemName));
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + menuItemName.hashCode();
        return result;
    }

    public MenuItem() {
        super();
    }

    public MenuItem(int ID, int menuItemType, String menuItemName, float menuItemPrice) {
        this.ID = ID;
        this.menuItemType = menuItemType;
        this.menuItemName = menuItemName;
        this.menuItemPrice = menuItemPrice;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getMenuItemType() {
        return menuItemType;
    }

    public void setMenuItemType(int menuItemType) {
        this.menuItemType = menuItemType;
    }

    public float getMenuItemPrice() {
        return menuItemPrice;
    }

    public void setMenuItemPrice(float menuItemPrice) {
        this.menuItemPrice = menuItemPrice;
    }

    public String getMenuItemName() {
        return menuItemName;
    }

    public void setMenuItemName(String menuItemName) {
        this.menuItemName = menuItemName;
    }

    @Override
    public String toString() {
        return this.ID + " " + this.menuItemName + " " + this.menuItemType + " " + this.menuItemPrice;
    }
}
