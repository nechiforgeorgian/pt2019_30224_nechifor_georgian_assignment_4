package model;

public class LoginUser {
    private int ID;
    private int employeeID;
    private String username;
    private String passwrd;
    private int userType;

    public LoginUser() {
        super();
    }

    public LoginUser(int ID, int employeeID, String username, String passwrd, int userType) {
        super();
        this.ID = ID;
        this.employeeID = employeeID;
        this.username = username;
        this.passwrd = passwrd;
        this.userType = userType;
    }

    public int getID() {
        return ID;
    }

    public int getEmployeeID() {
        return employeeID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPasswrd() {
        return passwrd;
    }

    public void setPasswrd(String passwrd) {
        this.passwrd = passwrd;
    }

    public int getUserType() {
        return userType;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public void setEmployeeID(int employeeID) {
        this.employeeID = employeeID;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }
}
