package model;

import java.util.Date;

public class Employee {
    private int ID;
    private String fullName;
    private Date hiringDate;
    private String email;


    public Employee() {
        super();
    }

    public Employee(int ID, String fullName, Date hiringDate, String email) {
        super();
        this.ID = ID;
        this.fullName = fullName;
        this.hiringDate = hiringDate;
        this.email = email;
    }

    public int getID() {
        return ID;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Date getHiringDate() {
        return hiringDate;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public void setHiringDate(Date hiringDate) {
        this.hiringDate = hiringDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
