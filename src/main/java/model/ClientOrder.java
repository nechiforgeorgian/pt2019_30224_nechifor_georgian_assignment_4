package model;

import java.util.Date;
import java.util.Objects;

public class ClientOrder {
    private int ID;
    private Date orderDate;
    private int tableNumber;
    private float totalPrice;

    @Override
    public int hashCode() {
        int result = 15;
        result = 31 * result + this.ID;
        result = 31 * result + this.orderDate.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj)
            return true;
        if(obj == null || obj.getClass() != this.getClass())
            return false;
        ClientOrder clientOrder = (ClientOrder) obj;
        return (clientOrder.ID == this.ID && clientOrder.orderDate == this.orderDate);
    }

    public ClientOrder() {
        super();
    }

    public ClientOrder(int clientOrderID, Date orderDate, int tableNumber, float totalPrice) {
        this.ID = clientOrderID;
        this.orderDate = orderDate;
        this.tableNumber = tableNumber;
        this.totalPrice = totalPrice;
    }

    public int getID() {
        return ID;
    }

    public void setID(int id) {
        this.ID = id;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public int getTableNumber() {
        return tableNumber;
    }

    public void setTableNumber(int tableNumber) {
        this.tableNumber = tableNumber;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }
}
