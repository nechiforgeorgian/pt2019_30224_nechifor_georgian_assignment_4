package model;

public class ClientOrderDetails {
    private int ID;
    private int clientOrderID;
    private int menuItemID;
    private int menuItemAmount;
    private float menuItemPrice;

    public ClientOrderDetails() {
        super();
    }

    public ClientOrderDetails(int ID, int clientOrderID, int menuItemID, int menuItemAmount, float menuItemPrice) {
        this.ID = ID;
        this.clientOrderID = clientOrderID;
        this.menuItemID = menuItemID;
        this.menuItemAmount = menuItemAmount;
        this.menuItemPrice = menuItemPrice;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getClientOrderID() {
        return clientOrderID;
    }

    public void setClientOrderID(int clientOrderID) {
        this.clientOrderID = clientOrderID;
    }

    public int getMenuItemID() {
        return menuItemID;
    }

    public void setMenuItemID(int menuItemID) {
        this.menuItemID = menuItemID;
    }

    public int getMenuItemAmount() {
        return menuItemAmount;
    }

    public void setMenuItemAmount(int menuItemAmount) {
        this.menuItemAmount = menuItemAmount;
    }

    public float getMenuItemPrice() {
        return menuItemPrice;
    }

    public void setMenuItemPrice(float menuItemPrice) {
        this.menuItemPrice = menuItemPrice;
    }
}
