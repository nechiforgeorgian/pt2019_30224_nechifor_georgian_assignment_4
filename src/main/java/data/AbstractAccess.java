package data;

import java.lang.reflect.InvocationTargetException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;


public class AbstractAccess<T> {
    private final Class<T> type;

    @SuppressWarnings("unchecked")
    public AbstractAccess(Class<T> entityClass) {
        this.type = entityClass;
    }

    private String select(String field) {
        return "select * from " + type.getSimpleName() + " where " + field + " = ?";
    }

    private List<T> createObjects(ResultSet resultSet) throws SQLException {
        List<T> list = new ArrayList<>();
        try {
            while (resultSet.next()) {
                T instance = type.getDeclaredConstructor().newInstance();
                for (Field field : type.getDeclaredFields()) {
                    Object value = resultSet.getObject(field.getName());
                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
                    Method method = propertyDescriptor.getWriteMethod();
                    method.invoke(instance, value);
                }
                list.add(instance);
            }
        } catch (IllegalAccessException | InstantiationException | IntrospectionException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }
        return list;
    }

    public T getColumnByID(int id) {
        Connection connection = DatabaseFactory.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement(select("ID"));
            statement.setInt(1, id);
            statement.execute();
            ResultSet resultSet = statement.getResultSet();
            return createObjects(resultSet).get(0);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
