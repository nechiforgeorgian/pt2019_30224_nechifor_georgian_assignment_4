package data;

import model.ClientOrder;
import model.ClientOrderDetails;
import model.MenuItem;

import java.util.HashMap;
import java.util.List;

public interface RestaurantProcessing {


    /**
     * @pre menuitem != null
     * @post --
     */
     void newMenuItem(MenuItem menuItem);
    /**
     * @pre menuitem != null
     * @post menuItem.name = "null"
     */
     void deleteMenuItem(MenuItem menuItem);
    /**
     * @pre menuitem != null
     * @post final menuItem != init menuItem
     */
     void editMenuItem(MenuItem menuItem);

    /**
     * @pre order != null && menuItems.size() > 0
     * @post init count = final count + 1;
     */
     void newOrder(ClientOrder order, List<ClientOrderDetails> menuItems);
    /**
     * @pre order != null
     * @post @nochange order
     */
     float computeOrderPrice(ClientOrder order);
    /**
     * @pre order != null
     * @post @nochange order
     */
     void generateBill(ClientOrder order);
}
