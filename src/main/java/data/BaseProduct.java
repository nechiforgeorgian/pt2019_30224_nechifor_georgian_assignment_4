package data;

import model.MenuItem;

public class BaseProduct implements MenuItemData {
    @Override
    /**
     * @pre menuItem != null
     * @post @nochange
     */
    public float computePrice(MenuItem menuItem) {
        assert menuItem != null;
        return menuItem.getMenuItemPrice();
    }
}
