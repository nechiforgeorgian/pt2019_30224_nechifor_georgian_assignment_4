package data;

import businessLogic.AdminData;
import businessLogic.MenuItemBusiness;
import model.ClientOrder;
import model.ClientOrderDetails;
import model.MenuItem;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.List;


public class Restaurant {

    private HashMap<ClientOrder, List<ClientOrderDetails>> restaurantData = new HashMap<>();
    private Set<MenuItem> restaurantMenu = new HashSet<>();

    //ID, Type, Name, Price
    public void addMenu() throws FileNotFoundException {
        new MenuItemBusiness().deleteAllItems();
        File file = new File("menu.txt");
        Scanner scanner = new Scanner(file);

        while(scanner.hasNext()) {
            MenuItem menu = new MenuItem(scanner.nextInt(), scanner.nextInt(), scanner.next(), scanner.nextFloat());
            restaurantMenu.add(menu);
            new AdminData().newMenuItem(menu);
        }
    }

    public void addData(ClientOrder order, List<ClientOrderDetails> menuItems) {
        restaurantData.put(order, menuItems);
    }

    public List<ClientOrderDetails> getRestaurantOrders(ClientOrder order) {
        return restaurantData.get(order);
    }
}
