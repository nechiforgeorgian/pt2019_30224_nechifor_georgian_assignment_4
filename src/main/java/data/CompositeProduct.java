package data;

import model.MenuItem;

public class CompositeProduct implements MenuItemData {

    @Override
    /**
     * @pre menuitem !null
     * @post @nochange
     */
    public float computePrice(MenuItem menuitem) {
        assert menuitem != null;
        return menuitem.getMenuItemPrice();
    }
}
