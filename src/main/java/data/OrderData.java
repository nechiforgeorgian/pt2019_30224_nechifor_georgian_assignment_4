package data;

import model.ClientOrder;
import model.ClientOrderDetails;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class OrderData {

    public int getNumber() {
        int number = 0;
        Connection connection = DatabaseFactory.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("select count(*) from clientorder;");
            statement.execute();
            ResultSet resultSet = statement.getResultSet();
            while (resultSet.next()) {
                number = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return number;
    }

    public void newOrder(ClientOrder clientOrder, List<ClientOrderDetails> orderDetailsList) {
        Connection connection = DatabaseFactory.getConnection();
        int orderID = 0;
        float totalPrice = 0.0f;
        try {
            PreparedStatement statement = connection.prepareStatement("insert into clientOrder (orderDate, tableNumber, totalPrice) values (?, ?, ?);");
            statement.setString(1, (new SimpleDateFormat("yyyy-MM-dd")).format(new Date()));
            statement.setInt(2, clientOrder.getTableNumber());
            statement.setFloat(3, 0.0f);
            statement.execute();

            statement = connection.prepareStatement("select count(*) from clientorder");
            statement.execute();
            ResultSet resultSet = statement.getResultSet();
            while (resultSet.next()) {
                orderID = resultSet.getInt(1);
            }

            statement = connection.prepareStatement("insert into clientorderdetails (clientorderid, menuitemid, menuitemamount, menuitemprice) values(?, ?, ?, ?);");
            for(ClientOrderDetails clientOrderDetails: orderDetailsList) {
                statement.setInt(1, orderID);
                statement.setInt(2, clientOrderDetails.getMenuItemID());
                statement.setInt(3, clientOrderDetails.getMenuItemAmount());
                statement.setFloat(4, clientOrderDetails.getMenuItemPrice());
                statement.execute();

                totalPrice += clientOrderDetails.getMenuItemAmount() * clientOrderDetails.getMenuItemPrice();
            }

            statement = connection.prepareStatement("update clientorder set totalprice = ? where ID = ?");
            statement.setFloat(1, totalPrice);
            statement.setInt(2, orderID);
            statement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
