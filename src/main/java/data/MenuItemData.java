package data;

import model.MenuItem;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public interface MenuItemData {
     float computePrice(MenuItem menuItem);

     static int getNumber() {
          Connection connection = DatabaseFactory.getConnection();
          int number = 0;
          try {
               PreparedStatement statement = connection.prepareStatement("select count(*) from menuitem");
               statement.execute();
               ResultSet resultSet = statement.getResultSet();
               while (resultSet.next()) {
                    number = resultSet.getInt(1);
               }
          } catch (SQLException e) {
               e.printStackTrace();
          }
          return number;
     }
}
