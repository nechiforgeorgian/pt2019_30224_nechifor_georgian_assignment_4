package businessLogic;

import data.DatabaseFactory;
import data.MenuItemData;
import model.MenuItem;

import javax.xml.transform.Result;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MenuItemBusiness {

    public int getNumber() {
        return MenuItemData.getNumber();
    }

    public void newMenuItem(MenuItem menuItem, int flag) {
        assert menuItem != null;
        if(menuItem.getMenuItemName() != null && menuItem.getMenuItemType() > 0 && menuItem.getMenuItemPrice() > 0) {
            Connection connection = DatabaseFactory.getConnection();
            try {
                PreparedStatement statement = connection.prepareStatement("insert into menuitem (menuitemname, menuitemtype, menuitemprice) values (?, ?, ?)");
                statement.setString(1, menuItem.getMenuItemName());
                statement.setInt(2, menuItem.getMenuItemType());
                statement.setFloat(3, menuItem.getMenuItemPrice());
                statement.execute();

                statement = connection.prepareStatement("select count(*) from menuitem;");
                statement.execute();
                ResultSet resultSet = statement.getResultSet();
                while(resultSet.next()) {
                    menuItem.setID(resultSet.getInt(1));
                }
                if(flag == 1) {
                    String fileName = "menu.txt";
                    Files.write(Paths.get(fileName), (menuItem.getID() + " " + menuItem.getMenuItemType() + " " + menuItem.getMenuItemName() + " " + menuItem.getMenuItemPrice() + "\n").getBytes(), StandardOpenOption.APPEND);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void editMenuItem(MenuItem menuItem) {
        assert menuItem != null;
        if(menuItem.getMenuItemName() != null && menuItem.getMenuItemType() > 0 && menuItem.getMenuItemPrice() > 0) {
            Connection connection = DatabaseFactory.getConnection();
            try {
                PreparedStatement statement = connection.prepareStatement("update menuitem set menuitemname = ?, menuitemtype = ?, menuitemprice = ? where ID = ?;");
                statement.setString(1, menuItem.getMenuItemName());
                statement.setInt(2, menuItem.getMenuItemType());
                statement.setFloat(3, menuItem.getMenuItemPrice());
                statement.setInt(4, menuItem.getID());
                statement.execute();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void deleteMenuItem(MenuItem menuItem) {
        assert  menuItem != null;
        if(menuItem.getMenuItemName() != null && menuItem.getMenuItemType() > 0 && menuItem.getMenuItemPrice() > 0) {
            Connection connection = DatabaseFactory.getConnection();
            try {
                PreparedStatement statement = connection.prepareStatement("update menuItem set menuitemname = 'null' where ID = ?");
                statement.setInt(1, menuItem.getID());
                statement.execute();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void deleteAllItems() {
        Connection connection = DatabaseFactory.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("delete from menuitem where ID > 0;");
            statement.execute();
            statement.execute("alter table menuitem auto_increment = 1;");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
