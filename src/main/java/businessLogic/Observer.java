package businessLogic;

public interface Observer {
    void update(String obj);
}
