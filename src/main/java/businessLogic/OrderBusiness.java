package businessLogic;

import data.OrderData;
import model.ClientOrder;
import model.ClientOrderDetails;

import java.util.List;

public class OrderBusiness {

    public void newOrder(ClientOrder clientOrder, List<ClientOrderDetails> orderDetailsList) {
        assert clientOrder != null && orderDetailsList.size() > 0;
        new OrderData().newOrder(clientOrder, orderDetailsList);
    }
}
