package businessLogic;

import data.RestaurantProcessing;
import model.ClientOrder;
import model.ClientOrderDetails;
import model.MenuItem;

import java.util.List;

public class AdminData implements RestaurantProcessing {
    @Override
    public void newMenuItem(MenuItem menuItem) {
        assert (menuItem != null);
        new MenuItemBusiness().newMenuItem(menuItem, 0);
    }

    @Override
    public void deleteMenuItem(MenuItem menuItem) {
        assert !(menuItem == null);
        new MenuItemBusiness().deleteMenuItem(menuItem);

    }

    @Override
    public void editMenuItem(MenuItem menuItem) {
        assert menuItem != null;
        new MenuItemBusiness().editMenuItem(menuItem);
    }

    @Override
    public void newOrder(ClientOrder order, List<ClientOrderDetails> menuItems) {
        //waiter
    }
    @Override
    public float computeOrderPrice(ClientOrder order) {
        //waiter
        return 0;
    }
    @Override
    public void generateBill(ClientOrder order) {
        //waiter
    }
}
