package businessLogic;

import data.Restaurant;
import data.RestaurantProcessing;
import model.ClientOrder;
import model.ClientOrderDetails;
import model.MenuItem;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

public class WaiterData implements RestaurantProcessing {

    @Override
    public void newMenuItem(MenuItem menuItem) {
        //admin
    }

    @Override
    public void deleteMenuItem(MenuItem menuItem) {
        //admin
    }

    @Override
    public void editMenuItem(MenuItem menuItem) {
        //admin
    }

    @Override
    public void newOrder(ClientOrder order, List<ClientOrderDetails> orderDetails) {
        assert order != null && !orderDetails.isEmpty();
        new OrderBusiness().newOrder(order, orderDetails);
        new Restaurant().addData(order, orderDetails);
    }

    @Override
    public float computeOrderPrice(ClientOrder order) {
        assert order != null;
        return order.getTotalPrice();
    }

    @Override
    public void generateBill(ClientOrder order) {
        assert order != null;
        List<ClientOrderDetails> orderDetails = new Restaurant().getRestaurantOrders(order);
        String fileName = "bills.txt";
        try {
            Files.write(Paths.get(fileName), ( "Order ID: " + orderDetails.get(0).getID() + "\n").getBytes() , StandardOpenOption.APPEND);
            for(ClientOrderDetails orderDetails1: orderDetails) {
                Files.write(Paths.get("bills.txt"), ("\t" + "Product: " + orderDetails1.getMenuItemID() + "\t" + orderDetails1.getMenuItemPrice() + "\t" + "x" + orderDetails1.getMenuItemAmount() + "\n").getBytes() ,StandardOpenOption.APPEND);

            }

            Files.write(Paths.get(fileName), ("Total price: " + order.getTotalPrice() + "\n").getBytes() ,StandardOpenOption.APPEND);
            Files.write(Paths.get(fileName), ("---------------------------------------------------------------" + "\n").getBytes() ,StandardOpenOption.APPEND);


        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
