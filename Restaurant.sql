drop database if exists restaurant;
create database restaurant;

use restaurant;
create table userType (
	ID int auto_increment primary key,
    role varchar(20));
insert into userType (role) values ('Administrator'), ('Chef'), ('Waiter');

create table loginUser (
	ID int auto_increment primary key,
    employeeID int,
    username varchar(30),
    passwrd varchar(30),
    userType int);
insert into loginUser (employeeID, userName, passwrd, userType) values (1, 'admin', 'admin', 1), (2, 'chef', 'chef', 2), (3, 'waiter', 'waiter', 3);
    
create table employee (
	ID int auto_increment primary key,
    fullName varchar(100),
    hiringDate date,
    email varchar(40));
insert into employee (fullName, hiringDate, email) values ('Admin Diaconu', CURRENT_DATE(), 'admin@gmail.com'), 
('Chef Scarlatescu', CURRENT_DATE(), 'scarlatescu@gmail.com'), ('Waiter Dumitrescu', CURRENT_DATE(), 'dumitrescu@gmail.com');
    
create table clientOrder (
	ID int auto_increment primary key,
    orderDate date,
    tableNumber int,
    totalPrice float);
    
create table clientOrderDetails (
	ID int auto_increment primary key,
    clientOrderID int,
    menuItemID int,
    menuItemAmount int,
    menuItemPrice float);

create table menuItem (	
	ID int auto_increment primary key,
    menuItemName varchar(30),
    menuItemType int,
    menuItemPrice float);

create table menuItemProduct (
	ID int auto_increment primary key,
    itemType varchar(30));
insert into menuItemProduct (itemType) values ('BaseProduct'), ('CompositeProduct');


alter table loginUser
add foreign key (employeeID) references employee(ID) on delete cascade,
add foreign key (userType) references userType(ID) on delete cascade;

alter table clientOrderDetails
add foreign key (clientOrderID) references clientOrder(ID) on delete cascade,
add foreign key (menuItemID) references menuItem(ID) on delete cascade;

alter table menuItem
add foreign key (menuItemType) references menuItemProduct(ID) on delete cascade;

select * from menuitem;
delete from menuitem where ID > 0;
alter table menuitem auto_increment = 1;
update menuitem
set menuitemname = "Cartofi prajiti" where ID = 2;


insert into menuitem (menuitemname, menuitemtype, menuitemprice) values ('Meniu Pui si Cartofi', 2, 29.99), ('Cartofi Prajiti', 1, 3.50);
select * from clientorder;

select * from clientorderdetails;